class Comment < ApplicationRecord
  has_rich_text :body
  belongs_to :meeting
  belongs_to :user
end
